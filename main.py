from sunrise import Sunrise
from config import Config
import sunrise

c = Config()
sunrise = Sunrise(c)
sunrise.run_loop()