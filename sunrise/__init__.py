import time

import esp
import machine
import network
from machine import Pin
from neopixel import NeoPixel

from sunrise.mqtt import MQTTClient
from sunrise.simpletimer import SimpleTimer

esp.osdebug(None)
import gc

gc.collect()


def mapFromTo(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)


class Sunrise:
    def __init__(self, config):
        self.SSID_NAME = config.SSID_NAME
        self.SSID_PASSWORD = config.SSID_PASSWORD
        self.MQTT_SERVER = config.MQTT_SERVER
        self.MQTT_PORT = config.MQTT_PORT
        self.MQTT_USERNAME = config.MQTT_USERNAME
        self.MQTT_PASSWORD = config.MQTT_PASSWORD
        self.MQTT_CLIENT_NAME = config.MQTT_CLIENT_NAME
        # configure MQTT client
        self.mqtt_client = MQTTClient(
            self.MQTT_CLIENT_NAME,
            self.MQTT_SERVER,
            self.MQTT_PORT,
            self.MQTT_USERNAME,
            self.MQTT_PASSWORD,
        )
        self.LED_PIN = Pin(config.LED_PIN, Pin.OUT)
        self.NUM_LEDS = config.NUM_LEDS
        self.BRIGHTNESS = config.BRIGHTNESS
        self.SUNSIZE = config.SUNSIZE
        self.BOOT = True
        self.SUN = (self.SUNSIZE * self.NUM_LEDS) / 100
        self.AURORA = self.NUM_LEDS
        self.SUN_PHASE = 100
        self.WHITE_LEVEL = 100
        self.RED = 127
        self.GREEN = 127
        self.BLUE = 127
        self.WHITE = 127
        self.EFFECT = "off"
        self.WAKE_DELAY = 1000
        self.FADE_STEP = 98
        self.OLD_FADE_STEP = 0
        self.CURRENT_AURORA = 100
        self.OLD_AURORA = 0
        self.CURRENT_SUN = 100
        self.OLD_SUN = 0
        self.SUN_FADE_STEP = 98
        # initialize LED strip
        self.leds = NeoPixel(self.LED_PIN, self.NUM_LEDS, bpp=4)
        self.leds.fill((0, 0, 0, 0))
        self.leds.write()
        # initialize timer
        self.timer = SimpleTimer()

    def setup_wifi(self):
        station = network.WLAN(network.STA_IF)
        station.active(True)
        station.connect(self.SSID_NAME, self.SSID_PASSWORD)
        while not station.isconnected():
            pass
        print("WIFI Connection successful!")
        print("IP Address: %s" % (station.ifconfig()[0]))

    def connect_and_subscribe(self):
        print("Attempting MQTT connection")
        mqtt_client_name = self.MQTT_CLIENT_NAME.encode()
        self.mqtt_client.set_callback(self.callback)
        self.mqtt_client.connect()
        print("Connected to MQTT broker: %s" % (self.MQTT_SERVER))
        if self.BOOT:
            self.mqtt_client.publish(mqtt_client_name + b"/checkIn", b"Rebooted")
            self.BOOT = False
        if not self.BOOT:
            self.mqtt_client.publish(mqtt_client_name + b"/checkIn", b"Reconnected")

        # subscribe to topics
        self.mqtt_client.subscribe(mqtt_client_name + b"/command")
        self.mqtt_client.subscribe(mqtt_client_name + b"/effect")
        self.mqtt_client.subscribe(mqtt_client_name + b"/color")
        self.mqtt_client.subscribe(mqtt_client_name + b"/white")
        self.mqtt_client.subscribe(mqtt_client_name + b"/wakeAlarm")

    def increase_sun_phase(self):
        if self.SUN_PHASE < 100:
            self.SUN_PHASE += 1
            self.timer.set_timeout(self.WAKE_DELAY, self.increase_sun_phase)
            self.timer.set_timeout(self.WAKE_DELAY / 80, self.increase_fade_step)
            self.timer.set_timeout(self.WAKE_DELAY / 80, self.increase_sun_fade_step)

    def increase_sun_fade_step(self):
        if self.SUN_FADE_STEP < 98:
            self.SUN_FADE_STEP += 1
            self.timer.set_timeout(self.WAKE_DELAY / 80, self.increase_sun_fade_step)

    def increase_fade_step(self):
        if self.FADE_STEP < 98:
            self.FADE_STEP += 1
            self.timer.set_timeout(self.WAKE_DELAY / 80, self.increase_fade_step)

    def increase_white_level(self):
        if self.WHITE_LEVEL < 100:
            self.WHITE_LEVEL += 1
            self.timer.set_timeout(self.WAKE_DELAY, self.increase_white_level)

    def select_effect(self):
        if self.EFFECT == "sunrise":
            self.led_sunrise()
        if self.EFFECT == "mqttRGB":
            self.led_rgbw()
        if self.EFFECT == "off":
            self.leds.fill((0, 0, 0, 0))
            self.leds.write()

    def draw_sun(self):
        self.CURRENT_SUN = mapFromTo(self.SUN_PHASE, 0, 100, 0, self.SUN)
        if self.CURRENT_SUN % 2 != 0:
            self.CURRENT_SUN -= 1
        if self.CURRENT_SUN != self.OLD_SUN:
            self.SUN_FADE_STEP = 0

        sun_start = int((self.NUM_LEDS / 2) - (self.CURRENT_SUN / 2))
        new_sun_left = sun_start - 1
        new_sun_right = sun_start + self.CURRENT_SUN
        if new_sun_left >= 0 and new_sun_right <= self.NUM_LEDS and self.SUN_PHASE > 0:
            red_value = mapFromTo(self.SUN_FADE_STEP, 0, 100, 127, 255)
            white_value = mapFromTo(self.SUN_FADE_STEP, 0, 100, 0, self.WHITE_LEVEL)
            self.leds[new_sun_left] = (red_value, 25, 0, white_value)
            self.leds[new_sun_right] = (red_value, 25, 0, white_value)
        for i in range(sun_start, sun_start + self.CURRENT_SUN):
            self.leds[i] = (255, 64, 0, white_value)
        self.OLD_SUN = self.CURRENT_SUN
        self.leds.write()

    def draw_aurora(self):
        self.CURRENT_AURORA = mapFromTo(self.SUN_PHASE, 0, 100, 0, self.AURORA)
        if self.CURRENT_AURORA % 2 != 0:
            self.CURRENT_AURORA -= 1
        if self.CURRENT_AURORA != self.OLD_AURORA:
            self.FADE_STEP = 0
        sun_start = int((self.NUM_LEDS / 2) - (self.CURRENT_AURORA / 2))
        new_aurora_left = sun_start - 1
        new_aurora_right = sun_start + self.CURRENT_AURORA
        if new_aurora_left >= 0 and new_aurora_right <= self.NUM_LEDS:
            red_value = mapFromTo(self.FADE_STEP, 0, 100, self.WHITE_LEVEL, 127)
            green_value = mapFromTo(self.FADE_STEP, 0, 100, 0, 25)
            self.leds[new_aurora_right] = (red_value, green_value, 0, 0)
            self.leds[new_aurora_left] = (red_value, green_value, 0, 0)
        for i in range(sun_start, sun_start + self.CURRENT_AURORA):
            self.leds[i] = (127, 25, 0, 0)
        self.OLD_FADE_STEP = self.FADE_STEP
        self.OLD_AURORA = self.CURRENT_AURORA
        self.leds.write()

    def draw_ambient(self):
        self.leds.fill((self.WHITE_LEVEL, 0, 0, 0))
        self.leds.write()

    def led_sunrise(self):
        self.draw_ambient()
        self.draw_aurora()
        self.draw_sun()

    def led_rgbw(self):
        self.leds.fill((self.RED, self.GREEN, self.BLUE, self.WHITE))
        self.leds.write()

    def callback(self, topic, msg):
        d_topic, d_msg = topic.decode(), msg.decode()
        print("Message arrived: %s -> %s" % (d_topic, d_msg))
        if d_topic.endswith("command"):
            self.EFFECT = d_msg
            self.mqtt_client.publish(self.MQTT_CLIENT_NAME.encode() + b"/state", msg)
        if d_topic.endswith("wakeAlarm"):
            self.WHITE_LEVEL = 0
            self.SUN_PHASE = 0
            self.FADE_STEP = 0
            self.SUN_FADE_STEP = 0
            self.EFFECT = "sunrise"
            wake_delay = int(d_msg) * 10
            self.timer.set_timeout(wake_delay, self.increase_sun_phase)
            self.timer.set_timeout(wake_delay, self.increase_white_level)
            self.timer.set_timeout(wake_delay / 80, self.increase_fade_step)
            self.timer.set_timeout(wake_delay / 80, self.increase_sun_fade_step)
            self.mqtt_client.publish(
                self.MQTT_CLIENT_NAME.encode() + b"/command", b"sunrise", True
            )  # this sets a retained value to restore the sunrise in case of reconnect
            self.mqtt_client.publish(
                self.MQTT_CLIENT_NAME.encode() + b"/state", b"mqttRGB"
            )  # this is needed for the state in home assistant
        if d_topic.endswith("white"):
            self.WHITE = int(d_msg)
            self.mqtt_client.publish(
                self.MQTT_CLIENT_NAME.encode() + b"/whiteState", msg
            )
        if d_topic.endswith("color"):
            self.mqtt_client.publish(
                self.MQTT_CLIENT_NAME.encode() + b"/colorState", msg
            )
            # TODO: might need to modify for RGBW
            r, g, b = [int(c) for c in d_msg.split(",")]
            # red
            if r < 0 or 255 < r:
                return
            else:
                self.RED = r
            # green
            if g < 0 or 255 < g:
                return
            else:
                self.GREEN = g
            # blue
            if b < 0 or 255 < b:
                return
            else:
                self.BLUE = b

    @staticmethod
    def restart_and_reconnect():
        print("Failed to connect to MQTT broker. Reconnecting...")
        time.sleep(10)
        machine.reset()

    def run_loop(self):
        self.setup_wifi()

        try:
            self.connect_and_subscribe()
            while True:
                self.mqtt_client.check_msg()
                self.timer.run()
                self.select_effect()
        except OSError:
            self.restart_and_reconnect()
