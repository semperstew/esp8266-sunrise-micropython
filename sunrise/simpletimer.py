"""
simpletimer.py

SimpleTimer - A timer library for micropython.
Author: andrew.stewart@elderresearch.com

Adapted from the C++ SimpleTimer library 
written by mromani@ottotecnica.com.
Copyright (c) 2010 OTTOTECNICA Italy


This library is free software; you can redistribute it
and/or modify it under the terms of the GNU Lesser
General Public License as published by the Free Software
Foundation; either version 2.1 of the License, or (at
your option) any later version.

This library is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser
General Public License along with this library; if not,
write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
"""
import time

MAX_TIMERS = 10  # maximum number of timers

# set_timer() constants
RUN_FOREVER = 0
RUN_ONCE = 1

# deferred call constants
DEFCALL_DONTRUN = 0  # don't call the callback function
DEFCALL_RUNONLY = 1  # call the callback function but don't delete the timer
DEFCALL_RUNANDDEL = 2  # call the callback function and delete the timer


class Timer:
    def __init__(
        self,
        prev_millis=time.ticks_ms(),
        callback=None,
        params=(),
        delay_ms=0,
        max_num_runs=0,
        enabled=False,
    ):
        self.prev_millis = prev_millis  # value returned by the time.ticks_ms() function in the previous run() call
        self.callback = callback  # the callback function
        self.params = params  # callback parameters
        self.delay_ms = delay_ms  # delay value in milliseconds
        self.max_num_runs = max_num_runs  # number of runs to be executed
        self.enabled = enabled  # true if enabled
        self.num_runs = 0  # number of executed runs
        self.to_be_called = DEFCALL_DONTRUN  # deferred function call (sort of) - N.B.: only used in run()
        self.has_params = (
            True if params else False
        )  # true if callback takes a parameter


class SimpleTimer:
    def __init__(self):
        self.MAX_TIMERS = MAX_TIMERS
        self.num_timers = 0  # actual number of timers in use
        self.current_millis = time.ticks_ms()
        self.timers = [Timer()] * self.MAX_TIMERS

    def run(self):
        # get current time
        current_millis = time.ticks_ms()

        for timer in self.timers:
            timer.to_be_called = DEFCALL_DONTRUN

            # no callback == no timer, i.e. jump over empty slots
            if timer.callback is not None:
                # is it time to process this timer?
                if current_millis - timer.prev_millis >= timer.delay_ms:
                    # update time
                    timer.prev_millis += timer.delay_ms
                    # check if the timer callback has to be executed
                    if timer.enabled:
                        # "run forever" timers must always be executed
                        if timer.max_num_runs == RUN_FOREVER:
                            timer.to_be_called = DEFCALL_RUNONLY
                        # other timers get executed the specified number of times
                        elif timer.num_runs < timer.max_num_runs:
                            timer.to_be_called = DEFCALL_RUNONLY
                            timer.num_runs += 1
                            # after the last run, delete the timer
                            if timer.num_runs >= timer.max_num_runs:
                                timer.to_be_called = DEFCALL_RUNANDDEL

        for i, timer in enumerate(self.timers):
            if timer.to_be_called == DEFCALL_DONTRUN:
                continue
            if timer.has_params:
                timer.callback(*timer.params)
            else:
                timer.callback()
            if timer.to_be_called == DEFCALL_RUNANDDEL:
                self.delete_timer(i)

    def setup_timer(
        self,
        delay_ms,
        max_num_runs,
        func=None,
        params=(),
    ):
        """Function to initialize and enable a new timer.

        Args:
            delay_ms (int): number of milliseconds between calls to `func`
            max_num_runs (int): number of times to call `func`
            func (callable, optional): function to call. Defaults to None.
            params (tuple, optional): arguments to pass to `func`. Defaults to ().

        Returns:
            int: timer number on success, else -1 on failure or no free timers
        """
        free_slot = self.find_first_free_slot()
        if free_slot < 0 or func is None:
            return -1

        self.timers[free_slot] = Timer(
            callback=func,
            params=params,
            delay_ms=delay_ms,
            max_num_runs=max_num_runs,
            enabled=True,
        )
        self.num_timers += 1
        return free_slot

    def set_interval(self, delay_ms, func, params=()):
        """Timer will call `func` with parameters `params` every
        `delay_ms` milliseconds forever.

        Args:
            delay_ms (int): number of milliseconds between calls to `func`
            func (callable): function to call
            params (tuple, optional): arguments to pass to `func`. Defaults to ().

        Returns:
            int: timer number on success, else -1 on failure or no free timers
        """
        return self.setup_timer(delay_ms, RUN_FOREVER, func, params)

    def set_timeout(self, delay_ms, func, params=()):
        """Timer will call `func` with parameters `params`
        after `delay_ms` milliseconds one time.

        Args:
            delay_ms (int): number of milliseconds to wait to call func
            func (callable): function to call
            params (tuple, optional): arguments to pass to func. Defaults to ().

        Returns:
            int: timer number on success, else -1 on failure or no free timers
        """
        return self.setup_timer(delay_ms, RUN_ONCE, func, params)

    def set_timer(self, delay_ms, max_num_runs, func, params=()):
        """Timer will call function `func` with parameters `params`
        every `delay_ms` milliseconds `max_num_runs` times.

        Args:
            delay_ms (int): number of milliseconds to wait to call `func`
            max_num_runs (int): number of times to call `func`
            func (callable): function to call
            params (tuple, optional): arguments to pass to func. Defaults to ().

        Returns:
            int: timer number on success, else -1 on failure or no free timers
        """
        return self.setup_timer(delay_ms, max_num_runs, func, params)

    def delete_timer(self, n):
        """Destroy the specified timer.

        Args:
            n (int): timer number
        """
        if n >= self.MAX_TIMERS:
            return

        # nothing to delete if no timers are in use
        if self.num_timers == 0:
            return

        # don't decrease the number of timers if the
        # specified slot is already empty
        if self.timers[n].callback is not None:
            self.timers[n] = Timer()
            self.num_timers -= 1

    def restart_timer(self, n):
        """Restart the specified timer.

        Args:
            n (int): timer number
        """
        if n >= self.MAX_TIMERS:
            return
        self.timers[n].prev_millis = time.ticks_ms()

    def is_enabled(self, n):
        """Check if specified timer is enabled.

        Args:
            n (int): timer number

        Returns:
            bool: True if specified timer is enabled, else False
        """
        if n >= self.MAX_TIMERS:
            return False
        return self.timers[n].enabled

    def enable(self, n):
        """Enable the specified timer.

        Args:
            n (int): timer number
        """
        if n >= self.MAX_TIMERS:
            return
        self.timers[n].enabled = True

    def disable(self, n):
        """Disable the specified timer.

        Args:
            n (int): timer number
        """
        if n >= self.MAX_TIMERS:
            return
        self.timers[n].enabled = False

    def toggle(self, n):
        """Enable the specified timer if it's currently disabled,
        and vice-versa.

        Args:
            n (int): timer number
        """
        if n >= self.MAX_TIMERS:
            return
        self.timers[n].enabled = not self.timers[n].enabled

    def get_num_timers(self):
        """Return the number of timers used."""
        return self.num_timers

    def get_num_available_timers(self):
        """Return the number of available timers."""
        return self.MAX_TIMERS - self.num_timers

    def find_first_free_slot(self):
        """Find first available slot.

        Returns:
            int: slot number of first empty slot
        """
        # all slots are used
        if self.num_timers >= self.MAX_TIMERS:
            return -1

        # return the first slot with no callback (i.e. free)
        for i, timer in enumerate(self.timers):
            if timer.callback is None:
                return i

        # no free slots found
        return -1